# Ansible Tips



## General Tips


1. ansible-playbook will look for group_vars dir but ansible command will not
source: https://docs.ansible.com/ansible/latest/inventory_guide/intro_inventory.html#organizing-host-and-group-variables


2. "True" and True are different in Ansible


## Connection

Ansible has all the SSH tooling available. If you use password package `sshpass` needs to be installed.
Consider this options for ansible.cfg:

```
[defaults]
ask_pass = True
ansible_user = ansible

```

Also available options: `ansible_password, become_user, become_pass, ask_become_pass` and others

Ansible by default is configured to use keys. 
It can't ask for key passphrase so consider using `ssh-agent`.